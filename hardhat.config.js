require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require("hardhat-tracer");


task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

const newAcc = ""
const apiKeys = {
  mumbai: "",
  bsc: "",
  rinkeby: ""
}
module.exports = {
  etherscan: {
    apiKey: apiKeys.bsc
  },
  defaultNetwork: "bsctestnet",
  networks: {
    hardhat:{ 
    },
    mumbai: {
      url: "https://speedy-nodes-nyc.moralis.io/7e9361d53693a6e439879bb5/polygon/mumbai",
      accounts: [newAcc]
    },
    bsctestnet: {
      url: "https://speedy-nodes-nyc.moralis.io/7e9361d53693a6e439879bb5/bsc/testnet",
      accounts: [newAcc]
    },
    mainnet: {
      url: "https://speedy-nodes-nyc.moralis.io/7e9361d53693a6e439879bb5/polygon/mainnet",
      accounts: [newAcc]
    }
  },
  solidity: {
    version: "0.8.6",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
};
