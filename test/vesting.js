const { expect } = require("chai");


describe("Token contract", function () {


  let token;
  let vesting;


  beforeEach(async function () {

    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const Token = await ethers.getContractFactory("Swaraj");
    const Vesting = await ethers.getContractFactory("TokenVesting");


    token = await Token.deploy();
    await token.deployed()

    vesting = await Vesting.deploy(token.address);


    await vesting.deployed()

    await token.transfer(vesting.address, ethers.utils.parseEther("1000000"))

  });

  // You can nest describe calls to create subsections.
  describe("Add rounds and userData", function () {
    // `it` is another Mocha function. This is the one you use to define your
    // tests. It receives the test name, and a callback function.

    // If the callback function is async, Mocha will `await` it.
    it("Should add rounds", async function () {
    
        await vesting.addRound(1,5,1642882000)
        await network.provider.send("evm_setNextBlockTimestamp", [1642882300])
        await network.provider.send("evm_mine") 
        let data  = await vesting.viewRoundData(1)
        expect(data).to.equal([5,1642882000,0,false])
    });

    it("Should add userdata", async function() {
        await vesting.addUserData(addr1.address, ethers.utils.parseEther("100"))
        let data  = await vesting.viewUserData(addr1.address);
        expect(data).to.equal([addr1.address, ethers.utils.parseEther("100")])
    })

});
})