// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";



contract TokenVesting is Ownable, ReentrancyGuard{

    using SafeMath for uint;
    using SafeERC20 for IERC20;

    IERC20 token;
    uint public totalTokensOwed;
    uint totalAllocation = 100;

    struct RoundSchema {
        uint percent;
        uint start;
        uint tokensClaimed;
        bool complete;
    }

    struct userInfoSchema {
        uint tokenOwed;
        uint previousClaimedRound;
        uint tokensClaimed;
    }
    
    mapping (address => mapping(uint => bool)) public roundsClaimed;
    mapping (uint => RoundSchema) public roundInfo;
    mapping (address => userInfoSchema) userInfo;

    constructor(IERC20 _token){
        token = _token;
    }

    function viewRoundData(uint _round) public view returns(RoundSchema memory){
        RoundSchema memory round = roundInfo[_round];
        return round;
    }

    function viewUserData(address _user) public view returns(userInfoSchema memory){
        userInfoSchema memory user = userInfo[_user];
        return user;
    }

    function viewDeficient() public view returns(uint) {
        
        uint balance = token.balanceOf(address(this));

        if(totalTokensOwed > balance){
            return totalTokensOwed.sub(balance);
        }
        return 0;
    }

    function addRound(
        uint _round,
        uint _percent, 
        uint _start
    ) external onlyOwner {
        require(_percent < totalAllocation, "Not Enough allocation points");
        RoundSchema memory round = RoundSchema(_percent, _start, 0, false);
        totalAllocation = totalAllocation.sub(_percent);
        roundInfo[_round] = round;
    }

    function addUserData(address _benificiary, uint _tokensOwed) external onlyOwner {

        userInfoSchema memory user = userInfoSchema(_tokensOwed, 0, 0);
        userInfo[_benificiary] = user;
        totalTokensOwed = totalTokensOwed.add(_tokensOwed);

    }

    function claimTokens(uint _round) public nonReentrant{

        RoundSchema memory round = roundInfo[_round];
        userInfoSchema memory user = userInfo[msg.sender];

        uint tokensToSend = user.tokenOwed.mul(round.percent).div(100);

        require(round.start <= block.timestamp, "Round has not started yet");
        require(tokensToSend <= user.tokenOwed, "More than what the contract owes");
        require(!roundsClaimed[msg.sender][_round], "Already claimed");
        
        round.tokensClaimed = round.tokensClaimed.add(tokensToSend);

        user.tokensClaimed = user.tokensClaimed.add(tokensToSend);
        roundsClaimed[msg.sender][_round] = true;
        totalTokensOwed = totalTokensOwed.sub(tokensToSend);
        user.previousClaimedRound = _round;

        token.safeTransfer(msg.sender, tokensToSend);

    }

}