//SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";

contract FundCollector is Ownable {

    uint minLimit;
    uint maxLimit;
    address payable wallet;
    bool lock = false;

    mapping(address => uint) public investments;

    event Deposit(address indexed depositor, uint amount);

    constructor(uint _minLimit, uint _maxLimit, address payable _wallet) {
        minLimit = _minLimit;
        maxLimit = _maxLimit;
        wallet = _wallet;
    }

    receive() payable external {
        require(!lock && msg.value >= minLimit && msg.value<= maxLimit);
        lock = true;
        
        (bool success, ) = wallet.call{value: msg.value}("");
        if (success) {
            emit Deposit(msg.sender, msg.value);
            investments[msg.sender] += msg.value;
        }
        lock = false;
        require(success, "Transfer failed.");
    }
}
