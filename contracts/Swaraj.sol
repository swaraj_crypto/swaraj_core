// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";

contract Swaraj is ERC20, Ownable {
    
    using SafeMath for uint;
    
    address tokenPair;
    uint taxPercent;
    bool enableTax;
    bool swapAndLiquifyEnabled = true;

    // Limit which determines if it's a whale transaction.
    uint antiWhaleLimit;
    
    // Number Of Tokens to add to liquidity (trigger)
    uint numTokensSellToAddToLiquidity = 100e18;

    address public charity = payable(address(0x81d41e24796A0Ff8D3D43cf55dc5fd9eF793949e));
    address public marketing = payable(address(0x629706f83721DD0F0D0faF2f6D0c70eEe0C4e7Df));

    struct NormalSell {
        uint marketing;
        uint charity;
        uint lp;
    }

    struct WhaleSell {
        uint marketing;
        uint charity;
        uint lp;
    }

    NormalSell public normalSell;
    WhaleSell public whaleSell;

    uint public _forMarketing;
    uint public _forLP;

    mapping(address => bool) public isExcluded;

    IUniswapV2Router02 public uniswapV2Router;
    address public uniswapV2Pair;

    bool inSwapAndLiquify;

    event MinTokensBeforeSwapUpdated(uint256 minTokensBeforeSwap);
    event SwapAndLiquifyEnabledUpdated(bool enabled);
    event SwapAndLiquify(
        uint256 tokensSwapped,
        uint256 ethReceived,
        uint256 tokensIntoLiqudity
    );

    modifier lockTheSwap() {
        inSwapAndLiquify = true;
        _;
        inSwapAndLiquify = false;
    }
    
    constructor() ERC20("Swaraj", "SWRJ") {
        _mint(msg.sender, 10000000000 * 10 ** decimals());

        IUniswapV2Router02 _uniswapV2Router = IUniswapV2Router02(
            0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3
        );

        uniswapV2Router = _uniswapV2Router;

        normalSell.charity = 0;
        normalSell.marketing = 50;
        normalSell.lp = 0;

        whaleSell.charity = 0;
        whaleSell.marketing = 500;
        whaleSell.lp = 100;

        isExcluded[address(this)] = true;
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }


    receive() external payable {}

    function calculateDistribution(uint _tAmount) internal view returns (uint,uint,uint,uint){
        if(_tAmount < antiWhaleLimit){
            uint charityAmount = _tAmount.mul(normalSell.charity).div(10000);
            uint marketingAmount = _tAmount.mul(normalSell.marketing).div(10000);
            uint lpAmount = _tAmount.mul(normalSell.lp).div(10000);
            uint totalTax = charityAmount.add(marketingAmount).add(lpAmount);
            return(totalTax,charityAmount,marketingAmount,lpAmount);
        }
        
        uint charityWhaleAmount = _tAmount.mul(whaleSell.charity).div(10000);
        uint marketingWhaleAmount = _tAmount.mul(whaleSell.marketing).div(10000);
        uint lpWhaleAmount = _tAmount.mul(whaleSell.lp).div(10000);
        uint totalWhaleTax = charityWhaleAmount.add(marketingWhaleAmount).add(lpWhaleAmount);
        return(totalWhaleTax,charityWhaleAmount,marketingWhaleAmount,lpWhaleAmount);
    }

    function swapAndSendFee(uint256 amount) private lockTheSwap {
        uint256 initialBalance = address(this).balance;
        swapTokensForMatic(amount);
        uint256 newBalance = address(this).balance.sub(initialBalance);

        payable(marketing).transfer(newBalance);
    }

    function swapAndLiquify(uint256 contractTokenBalance) private lockTheSwap {
        // split the contract balance into halves
        uint256 half = contractTokenBalance.div(2);
        uint256 otherHalf = contractTokenBalance.sub(half);

        // capture the contract's current ETH balance.
        // this is so that we can capture exactly the amount of ETH that the
        // swap creates, and not make the liquidity event include any ETH that
        // has been manually sent to the contract
        uint256 initialBalance = address(this).balance;

        // swap tokens for ETH
        swapTokensForMatic(half); // <- this breaks the ETH -> HATE swap when swap+liquify is triggered

        // how much ETH did we just swap into?
        uint256 newBalance = address(this).balance.sub(initialBalance);

        // add liquidity to uniswap
        addLiquidity(otherHalf, newBalance);

        emit SwapAndLiquify(half, newBalance, otherHalf);
    }

    function addLiquidity(uint256 tokenAmount, uint256 ethAmount) private {
        // approve token transfer to cover all possible scenarios
        _approve(address(this), address(uniswapV2Router), tokenAmount);

        // add the liquidity
        uniswapV2Router.addLiquidityETH{value: ethAmount}(
            address(this),
            tokenAmount,
            0, // slippage is unavoidable
            0, // slippage is unavoidable
            owner(),
            block.timestamp
        );
    }

    function swapTokensForMatic(uint256 tokenAmount) private {
        // generate the uniswap pair path of token -> weth
        address[] memory path = new address[](2);
        path[0] = address(this);
        path[1] = uniswapV2Router.WETH();
        _approve(address(this), address(uniswapV2Router), tokenAmount);

        // make the swap
        uniswapV2Router.swapExactTokensForETH(
            tokenAmount,
            0, // accept any amount of ETH
            path,
            address(this),
            block.timestamp
        );
    }

    function distributeFee() private {
        uint256 contractTokenBalance = balanceOf(address(this));

        bool overMinTokenBalance = contractTokenBalance >=
            numTokensSellToAddToLiquidity;

        if (
            overMinTokenBalance &&
            !inSwapAndLiquify &&
            swapAndLiquifyEnabled
        ) {

            swapAndSendFee(_forMarketing);
            swapAndLiquify(_forLP);

            _forMarketing = 0;
            _forLP = 0;
        }
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool){
        distributeFee();
        if(recipient == tokenPair && enableTax && !isExcluded[sender]){
            (uint taxAmount, uint charityTax, uint marketingTax, uint liquidityAmount) = calculateDistribution(amount);
            uint totalAmount = amount.add(taxAmount);
            require(totalAmount <= balanceOf(sender), "Not enough token balance for tax");

            _transfer(sender, recipient, amount);
            _transfer(sender, charity, charityTax);
            _transfer(sender, address(this), marketingTax.add(liquidityAmount));

            _forMarketing = _forMarketing.add(marketingTax);
            _forLP= _forLP.add(liquidityAmount);
            return true; 
        }

        super.transferFrom(sender, recipient, amount);
        return true;
    }

    function setSellTax(uint _percent) external onlyOwner {
        taxPercent = _percent;
    }

    function enableTaxes(bool _choice) external onlyOwner {
        enableTax = _choice;
    }

    function excludeAddress(address _payer) external onlyOwner {
        isExcluded[_payer] = true;
    }

    function setPairAddress(address _pair) external onlyOwner {
        tokenPair = _pair;
    }


    // Ownership will be transferred to DAO at a later point in time.
    function setWallets(address _charity, address _marketing) external onlyOwner {
        require(marketing != address(0) && charity != address(0), "Fee wallets can't be zero addresses");
        charity = _charity;
        marketing = _marketing;
    }

    function setAntiWhaleLimit(uint _antiWhaleLimit) external onlyOwner {
        antiWhaleLimit = _antiWhaleLimit;
    }

}
