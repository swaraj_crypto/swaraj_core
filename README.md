# swaraj_core

This repository contains all of the core Smart contracts in the swaraj ecosystem

This project uses hardhat as the framework to build and test our smart contracts. 

## Getting started

Try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```
