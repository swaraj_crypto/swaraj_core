// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.

const hre = require("hardhat");

async function main() {


  const MultiSigWallet = await hre.ethers.getContractFactory("MultiSigWallet");

  const multiSigWallet = await MultiSigWallet.deploy(
    [
      "0xC5E48C4403114D9f313Fb1F6BF82CE249ab48b9a",
      "0xF01f6FBaB7919f5414e133504a2955D3c1Cdd96F"
    ], 
    [
      "0x6F614e17569b73BE95C230Be3E8FcB27A2a148f2",
      "0x2ACeC43E0Bb6F7Fb37a208fbf8DBE6eC0F6Ba72C"
    ],
    [
      "0x902b9B7C5a117e1119D2423f5bd375a715Da8417",
      "0x61f6eB40f5744E9453528aa87dAeb0A01D200bCC"
    ],
    1
  );
  await multiSigWallet.deployed();

  console.log("multiSigWallet deployed to:", multiSigWallet.address);

}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
