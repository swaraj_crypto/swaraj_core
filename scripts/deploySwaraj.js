// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.

const hre = require("hardhat");

async function main() {


  const Token = await hre.ethers.getContractFactory("Swaraj");
  //const Vesting = await hre.ethers.getContractFactory("TokenVesting")

  const token = await Token.deploy();
  await token.deployed();

  const tokenHash = token.deployTransaction.hash;
  console.log(`Tx hash: ${tokenHash}\nWaiting for transaction to be mined...`);
  const tokenReceipt = await ethers.provider.waitForTransaction(tokenHash);

  console.log("Token address:", tokenReceipt.contractAddress);

  //const vesting = await Vesting.deploy(token.address)
  //await vesting.deployed()
  //console.log("Vesting deployed to: ", vesting.address)
/*
  await vesting.addRound(1, 5, 1642757400);
  await vesting.addRound(2, 8, 1642757700);
  await vesting.addRound(3, 8, 1642758000);
  await vesting.addRound(4, 8, 1642758300);
  await vesting.addRound(5, 8, 1642758600);
  await vesting.addRound(6, 8, 1642758900);
  await vesting.addRound(7, 8, 1642759200);
  await vesting.addRound(8, 8, 1642759500);
  await vesting.addRound(9, 8, 1642759800);
  await vesting.addRound(10, 8, 1642751010);
  await vesting.addRound(11, 8, 1642751040);
  await vesting.addRound(12, 8, 1642751070);
  await vesting.addRound(13, 7, 1642751100);



  console.log("Deploy complete")
  */

}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
