// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.

const hre = require("hardhat");

async function main() {


  const Collector = await hre.ethers.getContractFactory("FundCollector");

  const collector = await Collector.deploy("2000000000000000000", "10000000000000000000", "0x4B9ee4e668F8255A3CC6EaedfcbdA81Aa16Faa3e");
  await collector.deployed();

  console.log("collector deployed to:", collector.address);

}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
