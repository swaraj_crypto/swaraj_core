// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.

const { ethers } = require("ethers");
const hre = require("hardhat");

async function main() {

  let tkn = "0x9789E76a2591290c1dDb75E56bA2E5064FefeD3d"
  let pairAddr = "0x8a887fa05af1d563dd3620c26ee00182bfdf976f"
  let limit = ethers.utils.parseEther("1000")

  const Token = await hre.ethers.getContractFactory("Swaraj");
  const token = await Token.attach(tkn);

  await token.setPairAddress(pairAddr)
  await token.setAntiWhaleLimit(limit)
  await token.enableTaxes(true)

  //await token.distributeFee()
  console.log("Done!")

}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
